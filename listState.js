// ideally it should include something like lodash, it could solve lots and lots of my problems...
/** So it may have webpack, lodash, typescript, and something on testing i guess.. */

// i may have features sketchground on js, and something like solid implementation on typescript...
// configured accessors tip: if I want to retrieve object's current state the way as it described in getState, i can use this.getState() in .stateChanged() and .restoreState()
// configured accessros tip: configured accessor methods are supplied with stored state, stored list item and current list item
// feature: check list of primitives? = no arguments but list
// feature: reordered?

// need to provide much more test cases, split existing to different files, try different combinations
// 

// Lodash also could extend this functionallity


//  And now this entire thing is order independent


/**
 * it seems like i've reached some mvp for this idea, so i'd like to make some things: 
 * 
 * remote git, so i can develop it at different places
 */
export function getListState(list, ...fieldAccessors) {
  console.log('state of ', list, fieldAccessors);

  const stateMap = new Map();

  const stringAccessors = [];
  const configuredAccessors = [];

  fieldAccessors.forEach(accessor => {
    const accessorType = typeof accessor;

    switch (accessorType) {
      case 'string':
        stringAccessors.push(accessor);
        break;
      case 'object':
        for (const accessorKey in accessor) {
          if (accessor.hasOwnProperty(accessorKey)) {
            const accessorConfig = {
              stateProp: accessorKey,
              getState: accessor[accessorKey].getState,
              stateChanged: accessor[accessorKey].stateChanged,
              restoreState: accessor[accessorKey].restoreState,
            };
            configuredAccessors.push(accessorConfig);
          }
        }
        break;
      default:
        throw new Error(`Field accessor must be either string or object, has type '${accessorType}': ${accessor}`);
    }
  });

  const hasAccessors = !!(stringAccessors.length || configuredAccessors.length);

  console.log('resulting accessors', stringAccessors, configuredAccessors);

  list.forEach(item => {
    let itemState = {};

    if (hasAccessors) {
      stringAccessors.forEach(field => {
        itemState[field] = item[field];
      });

      configuredAccessors.forEach(accessor => {
        itemState[accessor.stateProp] = accessor.getState(item);
      });
    } else {
      itemState = item;
    }

    stateMap.set(item, itemState);
  });

  return {
    initialList: list,
    stateMap: stateMap,
    stringAccessors,
    configuredAccessors,
    changed() {
      if (this._isInitialListChanged()) {
        // this thing is also checks primitive lists
        return true;
      }

      if (hasAccessors) {
        for (const [listItem, itemState] of this.stateMap) {
          const hasStringAccessorChanged = !stringAccessors.every(stringAccessor => {
            return itemState[stringAccessor] === listItem[stringAccessor];
          });

          if (hasStringAccessorChanged) {
            return true;
          }

          const hasConfiguredAccessorChanged = !configuredAccessors.every(configuredAccessor => {
            const statedValue = itemState[configuredAccessor.stateProp];
            return !configuredAccessor.stateChanged({
              stagedValue: statedValue,
              listItem,
              itemState
            });
          });

          if (hasConfiguredAccessorChanged) {
            return true;
          }

        }
      }
      return false;
    },
    restore() {
      for (const [listItem, itemState] of this.stateMap) {
        stringAccessors.forEach(accessor => {
          listItem[accessor] = itemState[accessor];
        });

        configuredAccessors.forEach(accessor => {
          const statedValue = itemState[accessor.stateProp];
          accessor.restoreState({
            stagedValue: statedValue,
            listItem,
            itemState
          });
        });
      }

      if (this._isInitialListChanged()) {
        this.initialList.splice(0, this.initialList.length);

        for (const [listItem] of this.stateMap) {
          this.initialList.push(listItem);
        }
      }
    },
    _isInitialListChanged() {
      return !!difference(this.initialList, Array.from(this.stateMap.keys())).length;
    }
  }
}

class MutableListState {

  constructor(list, stringAccessors, configuredAccessors) {

  }
}

export function difference(itemIds, stateIds) {
  return itemIds
    .filter(itemId => !stateIds.includes(itemId))
    .concat(stateIds.filter(stateId => !itemIds.includes(stateId)));
}