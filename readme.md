# Brief Description

This is solution to a problem of checking and restoring previous object's states.
It's a home project of one developer, so there is no well structure yet.
But the development cycle is:

* There is a js playground where new features could be tried out and implemented

I want to improve this thing.
Want to create webpack config, make stable version in typescript, use lodash and write some tests on it.
Will try to make this work.

## Current Features

We may have mutable lists with changeable state of items or list itself (inserted items or dropped items).
To make it work, we pass list to getListState(list, fieldAccessors), so we can keep track of some fields and we can tell how to do it.

There are actually test cases. You may check them out, they should be illustrative and simple.

Advantages are that there is no deep clones. Plus it's configurable. We can teach state of how it should be stored, compared and restored.


## For lists:

* We can retrieve staged state of list by getListState(list, fieldAccessors)
* We can track items state in list (for example, if some field of list item has been changed)
* We can track if list changed it's structure, dropped staged elements or when it gets new ones
* We can track if nested sublists had changed
* We can restore staged state
* We can configure accessors, teach them how to remember state of item, how to compare and how to restore it
* We can keep track of lists of primitives
* We can combine configured accessors with getListState() to keep track of nested lists on objects/primitives

## Planned

* Hmm.. Keep track of objects...