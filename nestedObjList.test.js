import {
  getListState,
} from './listState.js';

// test data 
const data = [
  {
    name: 'peter',
    age: 13,
    films: [{
        id: '1',
        title: 'Lord of the Rings'
      },
      {
        id: '2',
        title: 'Lotr 2'
      }
    ]
  },
  {
    name: 'jackson',
    age: 53,
    films: [{
        id: '3',
        title: 'Lord of the Rings 5'
      },
      {
        id: '4',
        title: 'Lotr 0'
      }
    ]
  }
];

const listState = getListState(data, 'age', 'name', {
  ids: {
    getState(listItem) {
      return getListState(listItem.films, 'id', 'title');
    },
    stateChanged({stagedValue}) {
      return stagedValue.changed();
    },
    restoreState({stagedValue}) {
      stagedValue.restore();
    }
  }
});


console.assert(listState.changed() === false, 'if state is just created, than state is not changed');

data[1].films.push({ id : '55', title : 'whatever'});

console.assert(listState.changed() === true, 'if new item pushed to sublist, then state is changed');

data[1].films.pop();

console.assert(listState.changed() === false, `if pushed value has been removed from sublist, then state is not changed`);

const initialFilmId = data[1].films[1].id;
data[1].films[1].id = '999';

console.assert(listState.changed() === true, 'if existing film id different to remembered state, then state is changed');

data[1].films[1].id = initialFilmId;

console.assert(listState.changed() === false, 'if initial film id restored, then state is not changed');

data[1].name = 'jackson';

console.assert(listState.changed() === false, 'if new value equal to current value, then state is not changed');

data.push({});

console.assert(listState.changed() === true, 'if new item is defined in initial list, then state is changed');

data.pop();

console.assert(listState.changed() === false, 'if new item was removed from initial list, then state is not changed');

const droppedItem = data.pop();

console.assert(listState.changed() === true, 'if list drops an item that was existing during state creation, then state is changed');

data.push(droppedItem);

console.assert(listState.changed() === false, 'if list restores dropped item, then state is not changed');

data[1].name = '999';

listState.restore();

console.assert(listState.changed() === false, 'if state by string accessor is restored, then state is not changed');

data[1].films[1].id = '999';

listState.restore();

console.assert(listState.changed() === false, 'if state by configured accessor is restored, then state is not changed');

data.push({});
listState.restore();

console.assert(listState.changed() === false, 'if list has new items and gets restored, then state is not changed');


data.pop();
listState.restore();

console.assert(listState.changed() === false, 'if list has dropped some item that staged and state gets restored, then state is not changed');


data[0].films.push({});
listState.restore();

console.assert(listState.changed() === false, 'if sublist of list has new items and state gets restored, then state is not changed');

data[0].films.pop();
listState.restore();

console.assert(listState.changed() === false, 'if sublist of list has dropped staged items and state gets restored, then state is not changed');

console.log('list with objects after all changes: ', data);