import { getListState } from "./listState";


const data = getTestData();

const listState = getListState(data, 'isActive');



function getTestData() {
  return [
    {
      "id": 0,
      "isActive": true,
      "age": 721,
      "eyeColor": "green",
      "name": "Hatfield Murray",
      "gender": "male",
      "company": "TETAK"
    },
    {
      "id": 1,
      "isActive": true,
      "age": 618,
      "eyeColor": "blue",
      "name": "Evangeline Leonard",
      "gender": "female",
      "company": "GLASSTEP"
    },
    {
      "id": 2,
      "isActive": true,
      "age": 691,
      "eyeColor": "brown",
      "name": "Fulton Mason",
      "gender": "male",
      "company": "CORIANDER"
    },
    {
      "id": 3,
      "isActive": true,
      "age": 115,
      "eyeColor": "brown",
      "name": "Conner Thompson",
      "gender": "male",
      "company": "STEELFAB"
    },
    {
      "id": 4,
      "isActive": true,
      "age": 852,
      "eyeColor": "brown",
      "name": "Moore Flores",
      "gender": "male",
      "company": "OTHERSIDE"
    },
    {
      "id": 5,
      "isActive": true,
      "age": 188,
      "eyeColor": "blue",
      "name": "Bessie Owens",
      "gender": "female",
      "company": "MEDICROIX"
    }
  ]
}