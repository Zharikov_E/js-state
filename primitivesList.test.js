import { getListState } from './listState.js';


const data = [
  'We',
  'Wish',
  'You',
  'A',
  'Merry',
  'Cristmas',
];

// well, this is something to think about...
const listState = getListState(data);

console.assert(listState.changed() === false, 'list of primitives is just created, then state is not changed');

data[0] = 'abcabcabc';

console.assert(listState.changed() === true, 'if list item has been changed, then state is changed');

data[0] = 'We';

console.assert(listState.changed() === false, 'if list item returns to staged value, then state is not changed');

data.push('new value');

console.assert(listState.changed() === true, 'new item is pushed, then list state is changed');

data.pop();

console.assert(listState.changed() === false, 'new item has been removed, then list state is not changed')

const droppedItem = data.pop();

console.assert(listState.changed() === true, 'list dropped item that has been staged, then state is changed');

data.push(droppedItem);

console.assert(listState.changed() === false, 'dropped item has been restored, then state is not changed');

data[3] = 'other value';
listState.restore();

console.assert(listState.changed() === false, 'if staged item is lost and state gets restored, than state is not changed');

data.push('new text');
listState.restore();

console.assert(listState.changed() === false, 'if list has new item and state gets restored, than there is no changes');

data.pop();
listState.restore();

console.assert(listState.changed() === false, 'if list loses staged item and state gets restored, than state is not changed');

console.log('list with primitives after all changes', data);